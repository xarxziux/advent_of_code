#lang typed/racket
(require "input.rkt" "user_input.rkt")

(struct opcode
  ([code : Integer]
   [mode-1 : Integer]
   [mode-2 : Integer]
   [mode-3 : Integer]))

(struct state
  ([codes : (Vectorof Integer)]
   [pos : Integer]
   [success : Boolean]))

(: parse-opcode (Integer -> opcode))
(define (parse-opcode int)
  (opcode
   (modulo int 100)
   (modulo (floor (/ int 100)) 10)
   (modulo (floor (/ int 1000)) 10)
   (modulo (floor (/ int 10000)) 10)))

(: process-output (state opcode -> state))
(define (process-output st op)
  (let*
      ([codes : (Vectorof Integer) (state-codes st)]
       [pos : Integer (state-pos st)]
       [mode : Integer (opcode-mode-1 op)]
       [val : Integer
            (case mode
              [(0) (vector-ref codes (vector-ref codes (+ pos 1)))]
              [(1) (vector-ref codes (+ pos 1))]
              [else 0])])
    (case mode
      [(0 1)
       (begin
         (displayln
          (string-append
           "Diagnostic code "
           (number->string val)))
         (state codes (+ pos 2) #t))]
      [else (state codes pos #f)])))

(: process-input (Integer -> (state -> state)))
(define (process-input int)
  (λ (st) 
    (let*
        ([codes : (Vectorof Integer) (state-codes st)]
         [pos : Integer (state-pos st)]
         [slot : Integer (vector-ref codes (+ pos 1))]
         [val : Integer int])
      (vector-set! codes slot val)
      (state codes (+ pos 2) #t))))

(: process-math-code (state opcode (Integer Integer -> Integer) -> state))
(define (process-math-code st op fn)
  (let*
      ([codes : (Vectorof Integer) (state-codes st)]
       [pos : Integer (state-pos st)]
       [x : Integer
          (if
           (= (opcode-mode-1 op) 1)
           (vector-ref codes (+ pos 1))
           (vector-ref codes (vector-ref codes (+ pos 1))))]
       [y : Integer
          (if
           (= (opcode-mode-2 op) 1)
           (vector-ref codes (+ pos 2))
           (vector-ref codes (vector-ref codes (+ pos 2))))]
       [slot : Integer (vector-ref codes (+ pos 3))])
    (vector-set! codes slot (fn x y))
    (state codes (+ pos 4) #t)))

(: process-jump (state opcode Boolean -> state))
(define (process-jump st op flip?)
  (let*
      ([codes : (Vectorof Integer) (state-codes st)]
       [pos : Integer (state-pos st)]
       [is-zero? : Boolean
                 (if
                  (= (opcode-mode-1 op) 1)
                  (= 0 (vector-ref codes (+ pos 1)))
                  (= 0 (vector-ref codes (vector-ref codes (+ pos 1)))))]
       [val : Integer
            (if
             (= (opcode-mode-2 op) 1)
             (vector-ref codes (+ pos 2))
             (vector-ref codes (vector-ref codes (+ pos 2))))]            
       [jump? : Boolean (if flip? (not is-zero?) is-zero?)])
    (if
     jump?
     (state codes val #t)
     (state codes (+ pos 3) #t))))

(: process-compare (state opcode (Integer Integer -> Boolean) -> state))
(define (process-compare st op fn)
  (let*
      ([codes : (Vectorof Integer) (state-codes st)]
       [pos : Integer (state-pos st)]
       [x : Integer
          (if
           (= (opcode-mode-1 op) 1)
           (vector-ref codes (+ pos 1))
           (vector-ref codes (vector-ref codes (+ pos 1))))]
       [y : Integer
          (if
           (= (opcode-mode-2 op) 1)
           (vector-ref codes (+ pos 2))
           (vector-ref codes (vector-ref codes (+ pos 2))))]
       [slot : Integer (vector-ref codes (+ pos 3))]
       [val : Integer (if (fn x y) 1 0)])
    (vector-set! codes slot val)
    (state codes (+ pos 4) #t)))

(: read-instructions (->* (state) (Integer) Boolean))
(define (read-instructions st [init 0])
  (let*
      ([in-fn (process-input init)]
       [codes : (Vectorof Integer) (state-codes st)]
       [pos : Integer (state-pos st)]
       [err : Boolean (not (state-success st))]
       [op : opcode (parse-opcode (vector-ref codes pos))]
       [code : Integer (opcode-code op)])
    (if
     err
     #f
     (case code
       ([99] #t)
       ([1] (read-instructions (process-math-code st op +)))
       ([2] (read-instructions (process-math-code st op *)))
       ([3] (read-instructions (in-fn st)))
       ([4] (read-instructions (process-output st op)))
       ([5] (read-instructions (process-jump st op #t)))
       ([6] (read-instructions (process-jump st op #f)))
       ([7] (read-instructions (process-compare st op <)))
       ([8] (read-instructions (process-compare st op =)))
       (else #f)))))

(: main ((Vectorof Integer) Integer -> Boolean))
(define (main codes init)
  (read-instructions (state (vector-copy codes) 0 #t) init))
