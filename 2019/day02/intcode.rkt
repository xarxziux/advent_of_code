#!/usr/bin/env racket
#lang typed/racket
(require "../intcode_interpreter.rkt" "input.rkt")

(: setup-interpreter ((Vectorof Integer) -> interpreter-state))
(define (setup-interpreter init-codes)
  (init-interpreter init-codes))

(: loop-until-halt (interpreter-state -> Integer))
(define (loop-until-halt st)
  (if
   (is-halted? st)
   (get-pos-0 st)
   (loop-until-halt (resume-interpreter st '()))))

(: run-code ((Vectorof Integer) -> Integer))
(define (run-code codes)
  (loop-until-halt (setup-interpreter codes)))

(: get-first-code (Integer -> Integer))
(define (get-first-code x)
  (modulo (floor (/ x 100)) 100))

(: get-second-code (Integer -> Integer))
(define (get-second-code x)
  (floor (modulo x 100)))

(: init-match ((Vectorof Integer) Integer -> (Integer -> (U False Integer))))
(define (init-match init-codes target-score)
  (: find-match (Integer -> (U False Integer)))
  (define (find-match input)
    (if
     (> input 9999)
     #f
     (let
         ([reset-codes : (Vectorof Integer) (vector-copy init-codes)])
       (vector-set! reset-codes 1 (get-first-code input))
       (vector-set! reset-codes 2 (get-second-code input))
       (let
           ([answer : Integer (run-code reset-codes)])
         (if
          (= answer target-score)
          input
          (find-match (+ input 1)))))))
  find-match)

(: run-test ((Vectorof Integer) Integer Integer -> Void))
(define (run-test codes input expected)
  (let
      ([answer : Integer (run-code codes)])
    (if
     (= answer expected)
     (displayln "Test passed")
     (displayln (~a "Test failed: expected = " expected ", answer = " answer)))))

(: run-tests (-> Void))
(define (run-tests)
  (run-test test1 0 1)
  (run-test test2 0 3500)
  (run-test test3 0 2)
  (run-test test4 0 2)
  (run-test test5 0 2)
  (run-test test6 0 30)
  )

(: run-part-1 (-> Void))
(define (run-part-1)
  (let
      ([init-codes (get-codes)])
    (vector-set! init-codes 1 12)
    (vector-set! init-codes 2 2)
    (displayln (~a "The answer to part 1 is " (run-code (get-codes))))))

(: run-part-2 (-> Void))
(define (run-part-2)
  (let
      ([answer : (U False Integer) ((init-match (get-codes) 19690720) 0)])
    (if
     (boolean? answer)
     (displayln "No match found")
     (displayln (~a "The answer to part 2 is " answer)))))
               
(: main (-> Void))
(define (main)
  ;(run-tests)
  (run-part-1)
  (run-part-2)
  )

(main)
