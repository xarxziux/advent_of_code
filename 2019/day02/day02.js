const fs = require("fs");
let input;

const execute = (x, y) => {
    input = fs.readFileSync("input.txt", "utf8").split(",").map(x => x - 0);
    input[1] = x;
    input[2] = y;

    let atEndOfInput = false;
    let i = 0;

    do {
        atEndOfInput = nextIter(i);
        i = i + 4;
    } while (!atEndOfInput);
};

const nextIter = index => {
    const action = input[index];

    if (action == 99)
        return true;

    if (action != 1 && action != 2)
        throw new Error("Something went wrong");

    const v1 = input[input[index + 1]];
    const v2 = input[input[index + 2]];
    input[input[index + 3]] = action == 1
        ? v1 + v2
        : v1 * v2;

    return false;
};

const main = () => {
    execute(12, 2);

    console.log("Answer to part 1 = " + input[0] + ".");

    let foundIt = false;
    let i = 0;

    while (i < 100 && !foundIt) {
        let j = 0;
        while (j < 100 && !foundIt) {
            execute(i, j);
            j++;

            if (input[0] == 19690720)
                foundIt = true;
        }

        i++;
    }

    if (foundIt)
        console.log("Answer to part 2 = " + (input[1] * 100 + input[2]) + ".");
};

main();
