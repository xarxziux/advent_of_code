#lang typed/racket
(provide get-encoding)

(: get-encoding (-> (Listof Char)))
(define (get-encoding)
  (string->list
   (string-trim
    (port->string
     (open-input-file
      (string->path "input.txt"))))))

(: main (-> Void))
(define (main)
  (displayln (length (get-encoding))))
