import caveMap from './data';

const move = (dir, {row, col, mins}) => {
    const dx = (
        dir === 0
            ? 1
            : (dir === 2)
            ? -1
            : 0);

    const dy = (
        dir === 1
            ? 1
            : (dir === 3)
            ? -1
            : 0);

    
