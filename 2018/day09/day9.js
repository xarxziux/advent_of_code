const arrMax = arr => arr.reduce((x, y) => Math.max(x, y), 0);

const showPercentage = m => t => (
    console.log(`${((t * 100)/m).toFixed()}% complete`)
);

const insertMarble = (arr, currIndex, nextValue) => {
    if (arr.length === 1)
        return {arr: [0, 1], currIndex: 1};

    if (arr.length === 2)
        return {arr: [0, 2, 1],  currIndex: 1};

    const nextIndex = (currIndex === arr.length - 1 ? 1 : currIndex + 2);

    return {
        arr: arr.slice(0, nextIndex)
            .concat(nextValue)
            .concat(arr.slice(nextIndex)),
        currIndex: nextIndex
    };
};

const getScores = (elfCount, marbles) => {
    let arr = [0];
    let currIndex = 1;
    let currValue = 1;
    let currElf = 0;
    let scores = Array(elfCount).fill(0);
    let showProgress = showPercentage(marbles);

    while (currValue <= (marbles + 1)) {
        if (currValue % 100000 === 0)
            showProgress(currValue);

        if (currValue % 23 !== 0) {
            const nextIter = insertMarble(arr, currIndex, currValue);
            arr = nextIter.arr;
            currIndex = nextIter.currIndex;
            currValue += 1;
            currElf = (currElf + 1) % elfCount;
        } else {
            currIndex = (currIndex + arr.length - 7) % arr.length;
            scores[currElf] += currValue + arr[currIndex];
            arr = arr.slice(0, currIndex).concat(arr.slice(currIndex + 1));
            currValue += 1;
            currElf = (currElf + 1) % elfCount;
        };
    };

    return arrMax(scores);
};

/*
console.log(getScores(9, 25));
console.log(getScores(10, 1618));
console.log(getScores(13, 7999));
console.log(getScores(17, 1104));
console.log(getScores(21, 6111));
console.log(getScores(30, 5807));
console.log(getScores(476, 71657));
 */

console.log(getScores(476, 7165700));
