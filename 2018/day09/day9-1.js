function Node(x, next, prev) {
    this.value = x;
    this.next = next || this;
    this.prev = prev || this;
};

function CircularList(x) {
    this.current = new Node(x);
    this.size = 1;
}

CircularList.prototype.getNext = function() {
    this.current = this.current.next;
};

CircularList.prototype.getPrev = function() {
    this.current = this.current.prev;
};

CircularList.prototype.insert = function(x) {
    const newNode = new CircularList(x, this.current.next, this.current);

    this.current.next = newNode;
    this.current.next.prev = newNode;
    this.size += 1;
    return this;
};

CircularList.prototype.remove = function(x) {
    if (this.size === 1)
        throw new Error('Cannot remove another element');

    this.current.prev = this.current.next;
    this.current.next = this.current.prev;
    this.current = this.current.next;
    this.size -= 1;
    return this;
};

let base = new CircularList(0);
//base.insert(1);

console.log(base);

base.insert(1).insert(2).insert(3);

console.log(base);

//let newList = base.insert(1).ins;

console.log(base.size);
console.log(base.current.value);
console.log(base.current.next.value);
//console.log(base.current);
//console.log(base.current.next.next.value);
//console.log(base.current.next.next.next.value);
//console.log(base.current.next.next.next.next.value);

/*
console.log(base.getNext().getNext().getNext().value);
console.log(newList.value);
console.log(newList.getPrev().value);
 */
