#load "data.fs"
open Day12

let getSecond (xs: ('a * 'b)[]) (x: 'a): 'b when 'a: equality =
    Array.find (fun (s, _) -> s = x) xs
    |> snd

let pullFive (pad: 'a) (xs: 'a[]) (i: int): 'a[] =
    let l: int = Array.length xs
    match i with
    | _ when i < 2 -> Array.append (Array.create (2 - i) pad) xs.[ .. (i + 2)]
    | _ when l - i < 3 ->
        Array.create (i + 3 - l) pad
        |> Array.append xs.[(i - 2) .. ]
    | _ -> xs.[ (i - 2) .. (i + 2)]

let getNextGen (xs: char[]) (ts: (string * char)[]) (i: int): char =
    pullFive '.' xs i
    |> System.String
    |> getSecond ts

let iterateGen (xs: char[]) (ts: (string * char)[]): char[] =
    [| for i in 0 .. (Array.length xs) -> getNextGen xs ts i |]

let shiftLeft (xs: char[]): char[] =
    xs.[ 1 .. ]

let rec iterXGen (ts: (string * char)[]) (i: int) (xs: char[]): char[] =
    match i with
    | 0 -> xs
    | _ ->
        iterateGen xs ts
        |> shiftLeft
        |> iterXGen ts (i - 1)

let getEmptyArray (x: int): char[] =
    Array.create x '.'

// The pattern repeats from generation 111 on with the given data set
let getGenScore (gens: int64): int64 =
    match gens with
    | g when g >= 111L -> 2728L + (20L *(gens - 111L))
    | _ ->
        let pad: int = 40
        Array.concat [| getEmptyArray pad; init; getEmptyArray pad |]
        |> iterXGen nextGen (int gens)
        |> Array.mapi (fun (i: int) (c: char) ->
                       match c with '#' -> (i - pad + (int gens)) | _ -> 0)
        |> Array.sum
        |> int64

let partOneAnswer: int64 = getGenScore 20L
let partTwoAnswer: int64 = getGenScore 50000000000L

printfn "Part one answer = %i" partOneAnswer
printfn "Part two answer = %i" partTwoAnswer
