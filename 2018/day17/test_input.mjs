export const springX = 40;
export const xOffset = 30;
export const xMax = 20;
export const yMax = 10;

export const data = [
    ['x', [33, 1, 7]],
    ['x', [45, 3, 8]],
    ['y', [7, 33, 45]]
];
