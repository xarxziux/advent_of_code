import {springX, xOffset, xMax, yMax, data} from './input';
//import {springX, xOffset, xMax, yMax, data} from './test_input';

/*
const xOffset = 30;
const xMax = 300;
const yMax = 2000;
*/

const getEmptyArr = x => () => Array(x).fill(0);
const arr = Array(yMax).fill().map(getEmptyArr(xMax));

const showArr = arr => { console.log(arr.map(x => x.join('')).join('\n')); };

const fillRow = ([row, start, end])=> {
    for (let i = start; i <= end; i++)
        arr[row][i - xOffset] = 1;
};

const fillCol = ([col, start, end])=> {
    for (let i = start; i <= end; i++)
        arr[i][col - xOffset] = 1;
};

const throwError = msg => { throw new Error(msg); };

const addVein = ([dir, dims]) => {
    dir === 'x'
        ? fillCol(dims)
        : dir === 'y'
        ? fillRow(dims)
        : throwError('Invalid dir parameter');
};




//showArr(arr);
//arr[8][7] = 3;
//showArr(arr);
arr[0][springX - xOffset] = 2;
data.forEach(addVein);
//mainData.forEach(addVein);
showArr(arr);
