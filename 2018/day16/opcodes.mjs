const addr = ([r, a, b, c]) => {
    const ans = [...r];
    ans[c] = ans[a] + ans[b];
    return ans;
};

const addi = ([r, a, b, c]) => {
    const ans = [...r];
    ans[c] = ans[a] + b;
    return ans;
};

const mulr = ([r, a, b, c]) => {
    const ans = [...r];
    ans[c] = ans[a] * ans[b];
    return ans;
};

const muli = ([r, a, b, c]) => {
    const ans = [...r];
    ans[c] = ans[a] * b;
    return ans;
};

const banr = ([r, a, b, c]) => {
    const ans = [...r];
    ans[c] = ans[a] & ans[b];
    return ans;
};

const bani = ([r, a, b, c]) => {
    const ans = [...r];
    ans[c] = ans[a] & b;
    return ans;
};

const borr = ([r, a, b, c]) => {
    const ans = [...r];
    ans[c] = ans[a] | ans[b];
    return ans;
};

const bori = ([r, a, b, c]) => {
    const ans = [...r];
    ans[c] = ans[a] | b;
    return ans;
};

const seti = ([r, a, b, c]) => {
    const ans = [...r];
    ans[c] = ans[a];
    return ans;
};

const setr = ([r, a, b, c]) => {
    const ans = [...r];
    ans[c] = a;
    return ans;
};

const gtir = ([r, a, b, c]) => {
    const ans = [...r];
    ans[c] = a > ans[b] ? 1 : 0;
    return ans;
};

const gtri = ([r, a, b, c]) => {
    const ans = [...r];
    ans[c] = ans[a] > b ? 1 : 0;
    return ans;
};

const gtrr = ([r, a, b, c]) => {
    const ans = [...r];
    ans[c] = ans[a] > ans[b] ? 1 : 0;
    return ans;
};

const eqir = ([r, a, b, c]) => {
    const ans = [...r];
    ans[c] = a === ans[b] ? 1 : 0;
    return ans;
};

const eqri = ([r, a, b, c]) => {
    const ans = [...r];
    ans[c] = ans[a] === b ? 1 : 0;
    return ans;
};

const eqrr = ([r, a, b, c]) => {
    const ans = [...r];
    ans[c] = ans[a] === ans[b] ? 1 : 0;
    return ans;
};

const opcodes = [
    borr,
    setr,
    mulr,
    eqri,
    banr,
    bori,
    bani,
    gtri,
    addr,
    muli,
    addi,
    eqrr,
    gtir,
    eqir,
    seti,
    gtrr
];

const isEqualArr = (a1, a2) => (
    a1.length === 4 &&
        a2.length === 4 &&
        a1[0] === a2[0] &&
        a1[1] === a2[1] &&
        a1[2] === a2[2] &&
        a1[3] === a2[3]
);

const testSample = ({before, code, after}) => fn => (
    isEqualArr(
        after,
        fn([before, ...code.slice(1)]),
    )
);

const countTrue = (x, b) => b ? x + 1 : x;

export const getPasses = sample => (
    opcodes.map(testSample(sample))
);

export const countPasses = sample => (
    getPasses(sample)
        .reduce(countTrue, 0)
);

/*
const applyInstruction = (r, i) => {
    console.log(r);
    console.log(i);
    return opcodes[i[0]]([r, ...i.slice(1)])
};
 */

export const applyInstructions = instructions => (
    instructions.reduce((r, i) => opcodes[i[0]]([r,...i.slice(1)]),[0, 0, 0, 0])
);
