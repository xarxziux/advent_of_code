use std::cmp;

fn count_matching (s: &[u8], i: u8) -> usize {
    s.iter().filter(|&n| *n == i).count()
}

fn count_nearby (arr: &[[u8; 10]; 10], row: usize, col: usize, target: u8)
        -> usize {
    let col_min: usize = match col { 0 => 0, _ => col - 1,};
    let col_max: usize = cmp::min(col + 2, arr.len());

    let above: usize = match row {
        0 => 0,
        _ => count_matching(&arr[row - 1][col_min .. col_max], target),
    };

    let below: usize = match row {
        x if x + 1 >= arr.len() => 0,
        _ => count_matching(&arr[row + 1][col_min .. col_max], target),
    };

    let previous: usize = match col {
        0 => 0,
        _ => match arr[row][col - 1] {
            i if i == target => 1,
            _ => 0,
        },
    };

    let next: usize = match col {
        c if c + 1 == arr.len() => 0,
        _ => match arr[row][col + 1] {
            i if i == target => 1,
            _ => 0,
        },
    };

    above + below + previous + next

}

fn update_arr (arr: &[[u8; 10]; 10]) -> [[u8; 10]; 10] {
    let mut new_arr: [[u8; 10]; 10] = [[0; 10]; 10];

    for x in 0 .. 10 {
        for y in 0 .. 10 {
            //new_arr[x][y] = arr[x][y] + x as u8 + y as u8;
            new_arr[x][y] = match arr[x][y] {
                0 => match count_nearby(arr, x, y, 1) >= 3 {
                    true => 1,
                    false => 0
                },
                1 => match count_nearby(arr, x, y, 2) >= 3 {
                    true => 2,
                    false => 1,
                },
                2 => match count_nearby(arr, x, y, 1) >= 1 &&
                    count_nearby(arr, x, y, 2) >= 1 {
                    true => 2,
                    false => 0,
                },
                _ => panic!("Invalid value"),
            };
        }
    }

    new_arr
}

fn update_arr_n (arr: &[[u8; 10]; 10]) -> [[u8; 10]; 10] {
    let mut arr1: [[u8; 10]; 10] = *arr;

    for _x in 0 .. 10 {
        let arr2 = update_arr(&arr1);
        arr1 = arr2;
    }

    arr1
}

fn main() {
    let test_arr: [[u8; 10]; 10] = [
        [0,2,0,2,0,0,0,1,2,0],
        [0,0,0,0,0,2,1,2,2,1],
        [0,1,0,0,1,0,0,0,2,0],
        [0,0,1,2,0,0,0,0,0,2],
        [2,0,2,1,1,1,2,1,2,1],
        [0,0,0,2,0,1,1,0,0,0],
        [0,1,0,0,0,0,1,0,0,0],
        [1,1,0,0,0,2,1,0,2,1],
        [1,0,1,1,1,1,0,0,1,0],
        [0,0,0,2,0,1,0,0,1,0]
    ];

    let answer: [[u8; 10]; 10] = update_arr_n(&test_arr);
    let mut trees: usize = 0;
    let mut lumber: usize = 0;

    for x in 0 .. 10 {
        trees += count_matching(&answer[x], 1);
        lumber += count_matching(&answer[x], 2);
    }

    println!("{}", trees * lumber);
}
