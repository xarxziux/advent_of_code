#load "data.fs"
open Input

let countAdjacent (a: int[][]) (row: int) (col: int): (int * int * int) =
    let rowMax: int = Array.length a
    let colMax: int = Array.length
