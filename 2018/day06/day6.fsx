#load "data.fs"
open Data

let getDist (x1: int, y1: int) (x2: int, y2: int): int =
    abs(x2 - x1) + abs(y2 - y1)

let findClosest (coords: (int * point) list) (p: point) =
    let distances: (int * int) list =
        List.map (fun (x: int, y: point) -> (x, (getDist y p))) coords
    let minDist: int = List.min <| List.map snd distances
    let closest: (int * int) list =
        List.filter (fun (_: int, x: int) -> x = minDist) distances
    match List.length closest > 1 with
    | true -> 0
    | false -> fst closest.[0]

let getDistToAll (coords: point list) (p: point): int =
    List.sum <| List.map (getDist p) coords

let getAllDist (coords: point list): int[][] =
    let gd = getDistToAll coords
    [| for i in 0 .. 999 -> [| for j in 0 .. 999 -> gd (i, j) |] |]

let findAllClosest (coords: (int * point) list): int[] =
    let fc = findClosest coords
    [| for i in 0 .. 999 -> [| for j in 0 .. 999 -> fc (i, j) |] |]
    |> Array.reduce Array.append

let countMatching (arr: int[]) (x: int): int =
    Array.length <| Array.filter (fun e -> e = x) arr

let countAll (arr: int[]): int[] =
    [| for i in 1 .. 50 -> countMatching arr i |]

let partOneAnswer =
    findAllClosest input
    |> countAll
    |> Array.sort

let partTwoAnswer =
    List.map snd input
    |> getAllDist
    |> Array.reduce Array.append
    |> Array.filter (fun x -> x < 10000)
    |> Array.length

printfn "Distance count: %A" partOneAnswer
printfn "Region size: %i" partTwoAnswer
