open System
open System.IO

let input: int =
    File.ReadAllLines("input.txt")
    |> Array.map int
    |> Array.sum

printfn "%i" input
