open System
open System.IO

let getNextIndex (max: int) (x: int): int =
    match x + 1 < max with
    | true -> x + 1
    | false -> 0

let findDup (arr: int[]) =
    let getNextI = getNextIndex arr.Length
    let rec findDupRecur (index: int) (current: int) (accum: int Set): int =
        let nextFreq: int = current + arr.[index]
        let nextIndex: int = getNextI index
        match Set.contains nextFreq accum with
            | true -> nextFreq
            | false -> findDupRecur nextIndex nextFreq (accum.Add nextFreq)
    findDupRecur 0 0 Set.empty

let input: int[] =
    File.ReadAllLines("input.txt")
    |> Array.map int

findDup input
    |> printfn "First duplicate frequency is: %i."
