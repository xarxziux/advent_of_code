open System.IO

let isSameChar (c1: char) (c2: char): bool =
    (System.Char.ToLower c1) = (System.Char.ToLower c2)

let isOppositeCase (c1: char) (c2: char): bool =
    c1 <> c2 && (isSameChar c1 c2)

let react (xs: char list): char list =
    let rec reactRec (accum: char list) (rem: char list): char list =
        match rem with
        | [] -> accum
        | rh :: rt ->
            match accum with
            | [] -> reactRec [rh] rt
            | ah :: at ->
                match isOppositeCase ah rh with
                | true -> reactRec at rt
                | false -> reactRec (rh :: accum) rt
    reactRec [] xs

let removeCharAndReact (cs: char list) (c: char): int =
    List.filter (fun x -> not(isSameChar c x)) cs
    |> react
    |> List.length

let findSmallest (c1: char, i1: int) (c2: char, i2: int): char * int =
    match i1 > i2 with
    | true -> (c2, i2)
    | false -> (c1, i1)

let input: char list =
    System.IO.File.ReadAllLines("input.txt").[0]
    |> Seq.toList

let partOneAnswer = react input |> List.length

let partTwoAnswer: int =
    List.map System.Char.ToLower input
    |> Set.ofList
    |> Set.toList
    |> List.map (removeCharAndReact input)
    |> List.min

printfn "Elements remaining: %i" partOneAnswer
printfn "Smallest polymer length: %i" partTwoAnswer
