module Input

let pathRules: (char * char) list =
    [
        ('C', 'P')
        ('V', 'Q')
        ('T', 'X')
        ('B', 'U')
        ('Z', 'O')
        ('P', 'I')
        ('D', 'G')
        ('A', 'Y')
        ('R', 'O')
        ('J', 'E')
        ('N', 'S')
        ('X', 'H')
        ('F', 'L')
        ('S', 'I')
        ('W', 'Q')
        ('H', 'K')
        ('K', 'Q')
        ('E', 'L')
        ('Q', 'O')
        ('U', 'G')
        ('L', 'O')
        ('Y', 'G')
        ('G', 'I')
        ('M', 'I')
        ('I', 'O')
        ('A', 'N')
        ('H', 'O')
        ('T', 'O')
        ('H', 'U')
        ('A', 'I')
        ('B', 'R')
        ('V', 'T')
        ('H', 'M')
        ('C', 'A')
        ('B', 'G')
        ('L', 'Y')
        ('T', 'J')
        ('A', 'R')
        ('X', 'L')
        ('B', 'L')
        ('A', 'F')
        ('K', 'O')
        ('W', 'M')
        ('Z', 'N')
        ('Z', 'S')
        ('R', 'K')
        ('Q', 'L')
        ('G', 'O')
        ('F', 'Y')
        ('V', 'H')
        ('E', 'I')
        ('W', 'Y')
        ('U', 'I')
        ('F', 'K')
        ('M', 'O')
        ('Z', 'H')
        ('X', 'S')
        ('J', 'O')
        ('B', 'I')
        ('F', 'H')
        ('D', 'U')
        ('E', 'M')
        ('Z', 'X')
        ('P', 'L')
        ('W', 'H')
        ('C', 'D')
        ('A', 'X')
        ('Q', 'I')
        ('R', 'Y')
        ('B', 'A')
        ('N', 'L')
        ('H', 'G')
        ('Y', 'M')
        ('L', 'G')
        ('G', 'M')
        ('Z', 'R')
        ('S', 'Q')
        ('P', 'J')
        ('V', 'J')
        ('J', 'I')
        ('J', 'X')
        ('W', 'O')
        ('B', 'F')
        ('R', 'M')
        ('V', 'S')
        ('H', 'E')
        ('E', 'U')
        ('R', 'W')
        ('X', 'Q')
        ('N', 'G')
        ('T', 'I')
        ('L', 'M')
        ('H', 'I')
        ('U', 'M')
        ('C', 'H')
        ('P', 'H')
        ('J', 'F')
        ('A', 'O')
        ('X', 'M')
        ('H', 'L')
        ('W', 'K')
    ]

let disallowedPaths: (char * char) list =
    [
        ('P', 'C')
        ('Q', 'V')
        ('X', 'T')
        ('U', 'B')
        ('O', 'Z')
        ('I', 'P')
        ('G', 'D')
        ('Y', 'A')
        ('O', 'R')
        ('E', 'J')
        ('S', 'N')
        ('H', 'X')
        ('L', 'F')
        ('I', 'S')
        ('Q', 'W')
        ('K', 'H')
        ('Q', 'K')
        ('L', 'E')
        ('O', 'Q')
        ('G', 'U')
        ('O', 'L')
        ('G', 'Y')
        ('I', 'G')
        ('I', 'M')
        ('O', 'I')
        ('N', 'A')
        ('O', 'H')
        ('O', 'T')
        ('U', 'H')
        ('I', 'A')
        ('R', 'B')
        ('T', 'V')
        ('M', 'H')
        ('A', 'C')
        ('G', 'B')
        ('Y', 'L')
        ('J', 'T')
        ('R', 'A')
        ('L', 'X')
        ('L', 'B')
        ('F', 'A')
        ('O', 'K')
        ('M', 'W')
        ('N', 'Z')
        ('S', 'Z')
        ('K', 'R')
        ('L', 'Q')
        ('O', 'G')
        ('Y', 'F')
        ('H', 'V')
        ('I', 'E')
        ('Y', 'W')
        ('I', 'U')
        ('K', 'F')
        ('O', 'M')
        ('H', 'Z')
        ('S', 'X')
        ('O', 'J')
        ('I', 'B')
        ('H', 'F')
        ('U', 'D')
        ('M', 'E')
        ('X', 'Z')
        ('L', 'P')
        ('H', 'W')
        ('D', 'C')
        ('X', 'A')
        ('I', 'Q')
        ('Y', 'R')
        ('A', 'B')
        ('L', 'N')
        ('G', 'H')
        ('M', 'Y')
        ('G', 'L')
        ('M', 'G')
        ('R', 'Z')
        ('Q', 'S')
        ('J', 'P')
        ('J', 'V')
        ('I', 'J')
        ('X', 'J')
        ('O', 'W')
        ('F', 'B')
        ('M', 'R')
        ('S', 'V')
        ('E', 'H')
        ('U', 'E')
        ('W', 'R')
        ('Q', 'X')
        ('G', 'N')
        ('I', 'T')
        ('M', 'L')
        ('I', 'H')
        ('M', 'U')
        ('H', 'C')
        ('H', 'P')
        ('F', 'J')
        ('O', 'A')
        ('M', 'X')
        ('L', 'H')
        ('K', 'W')
    ]

let exclusionList: Map<char,char list> =
    Map.ofList [
        ('A', ['B'; 'C'])
        ('D', ['C'])
        ('E', ['H'; 'J'])
        ('F', ['A'; 'B'; 'J'])
        ('G', ['B'; 'D'; 'H'; 'L'; 'N'; 'U'; 'Y'])
        ('H', ['C'; 'F'; 'P'; 'V'; 'W'; 'X'; 'Z'])
        ('I', ['A'; 'B'; 'E'; 'G'; 'H'; 'J'; 'M'; 'P'; 'Q'; 'S'; 'T'; 'U'])
        ('J', ['P'; 'T'; 'V'])
        ('K', ['F'; 'H'; 'R'; 'W'])
        ('L', ['B'; 'E'; 'F'; 'H'; 'N'; 'P'; 'Q'; 'X'])
        ('M', ['E'; 'G'; 'H'; 'L'; 'R'; 'U'; 'W'; 'X'; 'Y'])
        ('N', ['A'; 'Z'])
        ('O', ['A'; 'G'; 'H'; 'I'; 'J'; 'K'; 'L'; 'M'; 'Q'; 'R'; 'T'; 'W'; 'Z'])
        ('P', ['C'])
        ('Q', ['K'; 'S'; 'V'; 'W'; 'X'])
        ('R', ['A'; 'B'; 'Z'])
        ('S', ['N'; 'V'; 'X'; 'Z'])
        ('T', ['V'])
        ('U', ['B'; 'D'; 'E'; 'H'])
        ('W', ['R'])
        ('X', ['A'; 'J'; 'T'; 'Z'])
        ('Y', ['A'; 'F'; 'L'; 'R'; 'W'])
    ]
