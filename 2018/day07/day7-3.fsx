#load "data.fs"
open Input

let letters: char list = [ for i in 65 .. 90 -> char i ]

// Stolen from http://www.fssnip.net/4u/title/Very-Fast-Permutations
let rec insertions x = function
    | []             -> [[x]]
    | (y :: ys) as l -> (x::l)::(List.map (fun x -> y::x) (insertions x ys))

let rec permutations = function
    | []      -> seq [ [] ]
    | x :: xs -> Seq.concat (Seq.map (insertions x) (permutations xs))



type cl = char list
type ol = Option<char list>

let zipWith (xs: 'a list) (x: 'b): ('a * 'b) list =
    [ for i in 1 .. (List.length xs) -> x]
    |> List.zip xs

let rec containsMatch (xs: 'a list) (ys: 'a list): bool when 'a: equality =
    let rec matchOne (xs': 'a list) (y: 'a): bool =
        match xs' with
        | [] -> false
        | hxs' :: txs' ->
            match hxs' = y with
            | true -> true
            | false -> matchOne txs' y
    match ys with
    | [] -> false
    | hys :: tys ->
        match matchOne xs hys with
        | true -> true
        | false -> containsMatch xs tys

let canAdd (banList: (char * char) list) (xs: cl) (x: char): bool =
    zipWith xs x
    |> containsMatch banList
    |> not

let failsRule (xs: char list) (c1: char, c2: char): bool =
    (List.exists ((=) c1) xs) &&
    (List.exists ((=) c2) xs) &&
    (List.findIndex ((=) c1) xs) > (List.findIndex ((=) c2) xs)

let isBadPath (rules: (char * char) list) (xs: char list): bool =
    List.exists (failsRule xs) rules

let isGoodPath (rules: (char * char) list) (xs: char list): bool =
    not <| List.exists (failsRule xs) rules

let insertChar (filterFn: cl -> bool) (c: char) (xs: cl): cl list =
    List.filter filterFn <| insertions c xs

let insertChars (filterFn: cl -> bool) (accum: cl list) (c: char): cl list =
    List.map (insertChar filterFn c) accum
    |> List.reduce List.append

(*
let findPath (banList: (char * char) list) (elems: cl): ol =
    printfn "Finding a path for %A" elems
    let canAddInt = canAdd banList
    let rec findPathRec (accum: cl) (untested: cl) (rejected: cl): ol =
        match untested with
        | [] ->
            match rejected with
            | [] -> Some accum
            | _ -> None
        | h :: t ->
            match canAddInt accum h with
            | false -> findPathRec accum (List.append untested [h]) t
            | true -> findPathRec (List.append accum  [h]) [] t
    findPathRec [] [] elems

let findAllPaths (banList: (char * char) list) (elems: cl) =
    let findPathInt = findPath banList
    let rec findFirstPath (s: seq<char list>): char list =
        match findPathInt <| Seq.head s with
        | Some x -> x
        | None -> findFirstPath <| Seq.tail s
    findFirstPath <| permutations elems

let isBadPath (xs: char list) (c1: char, c2: char): bool =
    (List.findIndex ((=) c1) xs) > (List.findIndex ((=) c2) xs)

let passesAllRules (rules: (char * char) list) (xs: char list): bool =
    not <| List.exists (isBadPath xs) rules

let findAnswers (rules: (char * char) list) (elems: char list) =
    //let paths = permutations elems
    let ruleCheck = passesAllRules rules
    let rec findAnswerRec (paths: char list seq): char list =
        let h = Seq.head paths
        match ruleCheck h with
        | true -> h
        | false -> findAnswerRec (Seq.tail paths)
    let allPaths = permutations elems
    Seq.length allPaths
        //|> findAnswerRec
*)

    //findPathInt <| Seq.head paths
        //|> Seq.map findPathInt
        //|> Seq.find (fun x -> x.IsSome)
        //|> Some

    
        //|> Seq.toList
        //|> List.map findPathInt
    //paths
    
    //match paths elems with
    //| None -> []
    //| Some x -> x
    //paths







(*
let rec isAllowed (xs: (char * char) list) (x: char * char): bool =
    match xs with
    | [] -> true
    | h :: t ->
        match h = x with
        | true -> false
        | false -> isAllowed t x




    
    let fn = isAllowed banList
    let rec canAddRec
    match xs with
    | [] -> true
    | h :: t ->
        match fn (h, x) with
        | true -> false
        | false -> canAdd t x
*)

let partOneAnswer =
    findAnswers allowedPaths letters

printfn "%A" partOneAnswer
