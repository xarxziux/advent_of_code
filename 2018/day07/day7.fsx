#load "data.fs"
open Input

type cl = char list

let letters: char list = [ for i in 65 .. 90 -> char i ]

(*
let rec noMatchingElement (xs: 'a list) (ys: 'a list): bool when 'a: equality  =
    match ys with
    | [] -> true
    | h :: t ->
        match List.exists ((=) h) xs with
        | true -> false
        | false -> noMatchingElement xs t

let buildPath (fn: cl -> cl) (xs: cl): cl =
    let rec buildPathRec (accum: cl) (i: int) (rest: cl): option cl =
        match rest with
        | [] -> accum
        | _ ->
            match (=) i <| List.length rest
*)

let isBadPath (xs: char[]) (c1: char, c2: char): bool =
    (Array.exists ((=) c1) xs) &&
    (Array.exists ((=) c2) xs) &&
    (Array.findIndex ((=) c1) xs) > (Array.findIndex ((=) c2) xs)

let passesAllRules (rules: (char * char) list) (xs: char[]): bool =
    not <| List.exists (isBadPath xs) rules

let moveTail (xs: 'a[], ys: 'a[]): ('a[] * 'a[]) =
    match Array.length xs with
    | 0 -> failwith "This array should not be empty"
    | 1 -> ([||], Array.append xs ys)
    | _ ->
        (xs.[ .. (Array.length xs - 2) ],
         Array.append xs.[ (Array.length xs - 1) .. ] ys)

let insertChar (fn: char[] -> bool) (xs: char[]) (c: char): char[] =
    let rec insertCharInt (front: char[], back: char[]) (c: char): char[] =
        let a: char[] = Array.append front <| Array.append [|c|] back
        match fn a with
        | true -> a
        | false -> insertCharInt (moveTail (front, back)) c
    insertCharInt (xs, [||]) c






let partOneAnswer =
    findAnswers allowedPaths letters

printfn "%A" partOneAnswer
