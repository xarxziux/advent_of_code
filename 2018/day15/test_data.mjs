export const initCaveMap = [
    [0,0,0,0,0,0,0],
    [0,1,0,1,1,1,0],
    [0,1,1,1,0,0,0],
    [0,1,0,1,0,0,0],
    [0,1,1,0,0,0,0],
    [0,1,1,1,1,1,0],
    [0,0,0,0,0,0,0]
];

export const initGoblins = [
    {row: 1, col: 2, hp: 200, round: 0, id: 0},
    {row: 2, col: 5, hp: 200, round: 0, id: 1},
    {row: 3, col: 5, hp: 200, round: 0, id: 2},
    {row: 4, col: 3, hp: 200, round: 0, id: 3}
];

export const initElves = [
    {row: 2, col: 4, hp: 200, round: 0, id: 0},
    {row: 4, col: 5, hp: 200, round: 0, id: 1}
];
