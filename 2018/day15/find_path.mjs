import {astar, Graph} from './astar';

const findOneMove = (grid, start) => end => {
    const graph = new Graph(grid);
    const s = graph.grid[start.row][start.col];
    const f = graph.grid[end.row][end.col];
    const ans = astar.search(graph, s, f);

    return (ans.length === 0)
        ? null
        : {row: start.row, col: start.col, dist: ans[0].f};
};

const shortestDist = (d1, d2) => (
    d1.dist < d2.dist
        ? d1
        : d1.dist > d2.dist
        ? d2
        : d1.row < d2.row
        ? d1
        : d1.row > d2.row
        ? d2
        : d1.col < d2.col
        ? d1
        : d2
);

//*
export const findMoves = (grid, ends) => start => (
    ends.map(findOneMove(grid, start))
        .reduce(shortestDist)
);

export const findAllMoves = (grid, starts, ends) => (
    (starts.map(findMoves(grid, ends))).reduce(shortestDist)
);
// */

/*
export const findAllMoves = (grid, start, ends) => (
    (ends.map(findOneMove(grid, start))).sort(shortestDist)[0]
);
*/
