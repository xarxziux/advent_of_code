import {findAllMoves} from './find_path';

const getAdjacent = current => ([
    {row: current.row - 1, col: current.col},
    {row: current.row, col: current.col - 1},
    {row: current.row, col: current.col + 1},
    {row: current.row+ 1, col: current.col}
]);

const isFreeSquare = grid => square => (
    grid[square.row][square.col] === 1
);

export const chooseSquare = (current, enemies, grid) => {
    const baseSquares = getAdjacent(current)
              .filter(isFreeSquare(grid));

    if (baseSquares.length === 0)
        return null;

    const targetSquares = enemies
              .map(getAdjacent)
              .reduce((x, y) => x.concat(y))
              .filter(isFreeSquare(grid));

    if (targetSquares.length === 0)
        return null;

    return findAllMoves(grid, baseSquares, targetSquares);
};
