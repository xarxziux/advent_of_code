import {initCaveMap, initGoblins, initElves} from './test_data';
import {chooseTarget} from './choose_target';
import {chooseSquare} from './choose_square';

const findEnemyIndex = (enemies, {row, col}) => {
    if (enemies.length === 0)
        throw new Error('No enemy found');

    return (enemies[0].row === row && enemies[0].col === col)
        ? enemies[0].id
        : findEnemyIndex(enemies.slice(1), {row, col});
};

const attack = (grid, {row, col}, enemies) => {
    if (grid[row][col] !== 1)
        throw new Error('Invalid square parameter in attack() function');

    const i = findEnemyIndex(enemies, {row, col});

    enemies[i].hp = enemies[i].hp - 3;
};

const chooseAction = (grid, current, enemies) => {
    const target = chooseTarget(current, enemies);

    if (target != null) {
        attack(grid, target, enemies);
        current.round += 1;
        return;
    }

    const square = chooseSquare(current, enemies, grid);

    if (square == null) {
        current.round += 1;
        return;
    }

    grid[current.row][current.col] = 1;
    grid[square.row][square.col] = 0;
    current.row = square.row;
    current.col = square.col;
    current.round += 1;
    
};

//console.log(chooseAction({row: 1, col: 10}, testSample));
//console.log(chooseAction({row: 10, col: 10}, testSample));

const squares = chooseSquare({row: 1, col: 2}, initElves, initCaveMap);

console.log(squares);



//const chooseAction = (grid, entity, enemies) => {
    //const 



    
//let start = {row: 0, col: 0};
//let ends = [{row: 2, col: 1}];
//let ans = findAllMoves(testGrid, start, ends);

//console.log(ans);

