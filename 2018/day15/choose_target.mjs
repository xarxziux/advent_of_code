const isAdjacent = current => enemy => (
    (current.row === enemy.row || current.col === enemy.col) &&
        Math.abs(current.row + current.col - enemy.row - enemy.col) === 1
);

const findEnemy = (current, enemies) => enemies.filter(isAdjacent(current));

const readingOrder = (e1, e2) => (
    (e1.row - e2.row) || (e1.col - e2.col)
);

const chooseLowestHP = (enemies, accum = []) => (
    enemies.length === 0
        ? accum
        : accum.length === 0 || enemies[0].hp < accum[0].hp
        ? chooseLowestHP(enemies.slice(1), [enemies[0]])
        : enemies[0].hp > accum[0].hp
        ? chooseLowestHP(enemies.slice(1), accum)
        : chooseLowestHP(enemies.slice(1), accum.concat(enemies[0]))
);

export const chooseTarget = (current, enemies) => (
    (chooseLowestHP(findEnemy(current, enemies))).sort(readingOrder)[0]
);
