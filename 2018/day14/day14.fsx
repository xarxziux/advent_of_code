open System

type Recipe = {
    recipes: byte[];
    elf1: int
    elf2: int
}

let input = {
    recipes = [| 3uy; 7uy |];
    elf1 = 0;
    elf2 = 1
    }

let appendRecipe (a: byte[]) (x: byte): byte[] =
    match x > 9uy with
    | false -> Array.append a [| x |]
    | true -> Array.append a [| (x / 10uy); (x % 10uy) |]

let updateRecipe { recipes = a; elf1 = ei1; elf2 = ei2}: Recipe =
    let er1 = a.[ei1]
    let er2 = a.[ei2]
    let newRecipes: byte[] = appendRecipe a (er1 + er2)
    let l: int = Array.length newRecipes
    {
        recipes = newRecipes;
        elf1 = (int er1 + ei1 + 1) % l
        elf2 = (int er2 + ei2 + 1) % l
    }

let rec updateNRecipes (n: int) (r: Recipe): Recipe =
    let l: int = Array.length r.recipes
    if (l % 100000 = 0) then
        printfn "Calculated %i recipes" l
    let b: bool = n <= (Array.length r.recipes)
    match b with
    | true -> r
    | false -> updateNRecipes n (updateRecipe r)

let arrToStr (a: byte[]): string =
    Array.map (int >> sprintf "%A") a
    |> System.String.Concat

let getLast10Recipes (init: Recipe) (target: int): string =
    let allRecipes: Recipe = updateNRecipes (target + 15) init
    //IO.File.WriteAllText("test.txt", arrToStr allRecipes.recipes)
    arrToStr <| allRecipes.recipes.[ target .. (target + 9) ]

let getAnswer = getLast10Recipes input

let rec findIndex (target: string) (r: Recipe): int =
    let s: string = arrToStr r.recipes
    match s.Contains target with
    | true -> s.IndexOf target
    | false -> findIndex target (updateNRecipes 1000000 r)

printfn "Test 1 answer: %s" (getAnswer 9)
printfn "Test 2 answer: %s" (getAnswer 5)
printfn "Test 3 answer: %s" (getAnswer 18)
printfn "Test 4 answer: %s" (getAnswer 2018)
//printfn "Part 1 answer: %s" getAnswer 890691

//printfn "Part 2 test = %i" (findIndex "8176111038" input)
printfn "Part 2 test = %i" (findIndex "890691" input)
