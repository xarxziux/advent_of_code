fn split_recipe (x: u8) -> Vec<u8> {
    match x {
        x if x > 9 => vec![ x / 10, x % 10],
        _ => vec![x]
    }
}

fn update_n_recipes (n: usize) -> Vec<u8> {
    let mut l: usize = 0;
    let mut ei1: usize = 0;
    let mut er1: u8;
    let mut ei2: usize = 1;
    let mut er2: u8;
    let mut recipes: Vec<u8> = vec![3, 7];

    while l < (n + 10) {
        er1 = recipes[ei1];
        er2 = recipes[ei2];
        recipes.append(&mut split_recipe(er1 + er2));
        l = recipes.len();
        ei1 = (ei1 + (er1 as usize) + 1) % l;
        ei2 = (ei2 + (er2 as usize) + 1) % l;
    }

    recipes
}

fn get_last_10_recipes (n: usize) -> Vec<u8> {
    update_n_recipes(n)[n .. (n + 10)].to_vec()
}

fn find_seq (target: &[u8], limit: usize) -> usize {
    let mut big_vec: &[u8] = &(update_n_recipes(limit));
    let mut accum: usize = 0;
    let mut answer: usize = 0;

    while answer == 0 {
        let i: Option<usize> =
            big_vec.into_iter().position(|x| x == &target[0]);

        match i {
            None => break,
            Some(x) => accum = accum + x
        }

        big_vec = &big_vec[i.unwrap() .. ];

        if &big_vec[ .. target.len()] == target {
            answer = accum;
        }

        big_vec = &big_vec[1 .. ];
        accum += 1;
    }

    answer
}

fn main() {
    println!("Part 1, test 1: {:?}", get_last_10_recipes(9));
    println!("Part 1, test 2: {:?}", get_last_10_recipes(5));
    println!("Part 1, test 3: {:?}", get_last_10_recipes(18));
    println!("Part 1, test 4: {:?}", get_last_10_recipes(2018));
    println!("Part 1 answer: {:?}", get_last_10_recipes(890691));

    println!("Part 2, test 1: {}", find_seq(&[5, 1, 5, 8, 9], 50));
    println!("Part 2, test 2: {}", find_seq(&[0, 1, 2, 4, 5], 50));
    println!("Part 2, test 3: {}", find_seq(&[9, 2, 5, 1, 0], 50));
    println!("Part 2, test 4: {}", find_seq(&[5, 9, 4, 1, 4], 2100));
    println!("Part 2 answer: {}",  find_seq(&[8, 9, 0, 6, 9, 1], 30000000));
}
