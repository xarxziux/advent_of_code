#load "data.fs"
module Input

let rec consumeTree (xs: int[]): int[] * int =
    let subNodes = xs.[0]
    let meta = xs.[1]
    match xs0 with
    | 0 -> (xs.[ (xs1 + 2) .. ], Array.sum <| xs.[ 2 .. xs1 + 1])
    | _ -> 
