const fs = require('fs');
const puzzleData = fs.readFileSync('./input.txt', 'utf8')
          .split(' ')
          .map(x => x - 0);

const sum = arr => arr.length === 0 ? 0 : arr.reduce((a, b) => a + b);

const sumIndices = (arr, is) => (
    sum(is.map(x => arr[x - 1]).filter(x => x))
);

const consumeData = data => {
    let total = 0;
    let subNodeCount = data[0];
    const meta = data[1];
    let current = data.slice(2);

    while (subNodeCount > 0) {
        const {arr, accum} = consumeData(current);
        total += accum;
        current = [...arr];
        subNodeCount -= 1;
    }

    return {
        arr: current.slice(meta),
        accum: total + sum(current.slice(0, meta))
    };
};

const consumeData2 = data => {
    let i = 0;
    let nodeScores = [];
    let subNodeCount = data[0];
    const meta = data[1];
    let current = data.slice(2);

    if (subNodeCount === 0) {
        return {
            arr: current.slice(meta),
            accum: sum(current.slice(0, meta))
        };
    };

    while (i < subNodeCount) {
        const {arr, accum} = consumeData2(current);
        nodeScores[i] = accum;
        current = [...arr];
        i += 1;
    }

    return {
        arr: current.slice(meta),
        accum: sumIndices(nodeScores, current.slice(0, meta))
    };
};

let testData = [
    3, 5, 1, 3, 0, 4, 5, 2, 7, 1, 5, 6, 2, 2, 5, 0, 6, 3, 9, 6, 5, 7, 1, 0, 6,
    2, 1, 6, 9, 7, 4, 4, 2, 6, 5, 1, 0, 4, 3, 6, 7, 7, 6, 3, 9, 8, 5
];

let sampleData = [2, 3, 0, 3, 10, 11, 12, 1, 1, 0, 1, 99, 2, 1, 1, 2];

const partOneAnswer = data => consumeData(data).accum;
const partTwoAnswer = data => consumeData2(data).accum;

console.log(partOneAnswer(testData));
console.log(partOneAnswer(sampleData));
console.log(partOneAnswer(puzzleData));

console.log(partTwoAnswer(testData));
console.log(partTwoAnswer(sampleData));
console.log(partTwoAnswer(puzzleData));
