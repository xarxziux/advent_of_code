//import { readFileSync } from 'fs';
import fs from 'fs';

const input = fs.readFileSync('data.txt', 'utf8')
          .split('\n').filter(x => x.length > 0);
const maxRows = input.length;
const maxCols = input[0].length;
const carts = [];
const trackMap = Array(maxRows).fill().map(_ => Array(maxCols));;

const initCart = (row, col, dir) => {
    carts[carts.length] = {
        row,
        col,
        dir,
        choice: 0
    };
};

const sortCarts = (c1, c2) => (c1.row - c2.row || c1.col - c2.col);

const initTrackMap = data => {
    for (let row = 0; row < maxRows; row++) {
        for (let col = 0; col < maxCols; col++) {
            if (data[row][col] === ' ')
                trackMap[row][col] = 0;
            else if (data[row][col] === '-')
                trackMap[row][col] = 1;
            else if (data[row][col] === '|')
                trackMap[row][col] = 2;
            else if (data[row][col] === '/')
                trackMap[row][col] = 3;
            else if (data[row][col] === '\\')
                trackMap[row][col] = 4;
            else if (data[row][col] === '+')
                trackMap[row][col] = 5;
            else if (data[row][col] === '>') {
                trackMap[row][col] = 11;
                initCart(row, col, 0);
            } else if (data[row][col] === 'v') {
                trackMap[row][col] = 12;
                initCart(row, col, 1);
            } else if (data[row][col] === '<') {
                trackMap[row][col] = 11;
                initCart(row, col, 2);
            } else if (data[row][col] === '^') {
                trackMap[row][col] = 12;
                initCart(row, col, 3);
            } else {
                console.log(`row = ${row}, col = ${col}`);
                throw new Error(
                    `Invalid track character found: ${data[row][col]}`);
            }
        }
    }
};

const updateCart = cart => {
    let nextRow;
    let nextCol;

    if (cart.dir === 0) {
        nextRow = cart.row + 1;
        nextCol = cart.col;
    } else if (cart.dir === 1) {
        nextRow = cart.row;
        nextCol = cart.col + 1;
    } else if (cart.dir === 2) {
        nextRow = cart.row - 1;
        nextCol = cart.col;
    } else if (cart.dir === 3) {
        nextRow = cart.row;
        nextCol = cart.col - 1;
    }

    const nextTrack = trackMap[nextRow][nextCol];

    if (trackMap > 9)
        return `${nextCol},${nextRow}`;

    trackMap[cart.row][cart.col] = trackMap[cart.row][cart.col] % 10;
    trackMap[nextRow][nextCol] = nextTrack + 10;
    cart.row = nextRow;
    cart.col = nextCol;

    if (nextTrack === 1 || nextTrack === 2)
        return null;

    if (nextTrack === 3) {
        if (cart.dir === 0)
            cart.dir = 3;
        else if (cart.dir === 1)
            cart.dir = 2;
        else if (cart.dir === 2)
            cart.dir = 1;
        else
            cart.dir = 0;
        return null;
    }

    if (nextTrack === 4) {
        if (cart.dir === 0)
            cart.dir = 1;
        else if (cart.dir === 1)
            cart.dir = 0;
        else if (cart.dir === 2)
            cart.dir = 3;
        else
            cart.dir = 2;
        return null;
    }

    if (nextTrack === 5) {
        cart.choice = (cart.choice + 1) % 3;
};

const updateCarts = () => {
    carts.sort(sortCarts);

    carts.forEach(


};
console.log(maxRows);
console.log(maxCols);

initTrackMap(input);
console.log(input[0][0]);
console.log(carts);
