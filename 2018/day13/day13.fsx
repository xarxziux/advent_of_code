#load "data.fs"
open Day13

let moveCart {x = x; y = y; direction = d; choice = c}: Cart =
    match d with
    | Compass.East -> {x = x + 1; y = y;  direction = d; choice = c}
    | Compass.South -> {x = x; y = y + 1; direction = d; choice = c}
    | Compass.West -> {x = x - 1; y = y; direction = d; choice = c}
    | Compass.North -> {x = x; y = y - 1; direction = d; choice = c}
    | _ -> failwith "Invalid direction value"

let updateChoice (c: Choice): Choice =
    match c with
    | Choice.Left -> Choice.Straight
    | Choice.Straight -> Choice.Right
    | Choice.Right -> Choice.Left
    | _ -> failwith "Invalid Choice value"

let updateCart (a: byte[][])(cart: Cart)(dir: Choice)(onJunction: bool): Cart =
    match a.[cart.x].[cart.y] with
    | 1uy | 2uy -> cart
    | 3uy ->
        match cart.direction with
        | Compass.East ->
            {x=cart.x; y=cart.y; direction=Compass.North; choice=cart.choice}
        | Compass.South ->
            {x=cart.x; y=cart.y; direction=Compass.West; choice=cart.choice}
        | Compass.West ->
            {x=cart.x; y=cart.y; direction=Compass.South; choice=cart.choice}
        | Compass.North ->
            {x=cart.x; y=cart.y; direction=Compass.East; choice=cart.choice}
        | _ -> failwith "Invalid Cart value"
    | 4uy ->
        match cart.direction with
        | Compass.East ->
            {x=cart.x; y=cart.y; direction=Compass.South; choice=cart.choice}
        | Compass.South ->
            {x=cart.x; y=cart.y; direction=Compass.East; choice=cart.choice}
        | Compass.West ->
            {x=cart.x; y=cart.y; direction=Compass.North; choice=cart.choice}
        | Compass.North ->
            {x=cart.x; y=cart.y; direction=Compass.West; choice=cart.choice}
        | _ -> failwith "Invalid Cart value"
    | 5uy ->
        
