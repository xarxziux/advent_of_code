import {ip, instructions} from './data';

const registers = [0, 0, 0, 0, 0, 0];

const opCodes = {
    addr: ([a, b, c]) => {
        registers[c] = registers[a] + registers[b];
    },

    addi: ([a, b, c]) => {
        registers[c] = registers[a] + b;
    },

    mulr: ([a, b, c]) => {
        registers[c] = registers[a] * registers[b];
    },

    muli: ([a, b, c]) => {
        registers[c] = registers[a] * b;
    },

    banr: ([a, b, c]) => {
        registers[c] = registers[a] & registers[b];
    },

    bani: ([a, b, c]) => {
        registers[c] = registers[a] & b;
    },

    borr: ([a, b, c]) => {
        registers[c] = registers[a] | registers[b];
    },

    bori: ([a, b, c]) => {
        registers[c] = registers[a] | b;
    },

    setr: ([a, b, c]) => {
        registers[c] = registers[a];
    },

    seti: ([a, b, c]) => {
        registers[c] = a;
    },

    gtir: ([a, b, c]) => {
        registers[c] = a > registers[b] ? 1 : 0;
    },

    gtri: ([a, b, c]) => {
        registers[c] = registers[a] > b ? 1 : 0;
    },

    gtrr: ([a, b, c]) => {
        registers[c] = registers[a] > registers[b] ? 1 : 0;
    },

    eqir: ([a, b, c]) => {
        registers[c] = a === registers[b] ? 1 : 0;
    },

    eqri: ([a, b, c]) => {
        registers[c] = registers[a] === b ? 1 : 0;
    },

    eqrr: ([a, b, c]) => {
        registers[c] = registers[a] === registers[b] ? 1 : 0;
    }
};

export const applyInstructions = () => {
    const end = instructions.length;
    let count = 0;
    const max = 1000000000;
    let inst = 0;

    while (inst >= 0 && inst < end && count < max) {
        if (count % 1000000 === 0)
            console.log(`${count} iterations completed`);

        registers[ip] = inst;
        //process.stdout.write(registers.join(' ') + '\r');

        //console.log(`Before: [${registers}]`);
        //process.stdout.write(registers.join(',') + ',,');
        //const before = [...registers];

        const [fn, params] = instructions[inst];
        opCodes[fn](params);
        inst = registers[ip] + 1;

        //console.log(`After:registers}]`);
        //console.log(registers.join(','));
        if (inst === 28)
            console.log('' + count + ': ' + registers.join(','));

        count++;
    }

    return registers;
};
