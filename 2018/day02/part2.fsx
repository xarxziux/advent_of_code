open System
open System.IO

type Answer =
    | Found of char list * char list
    | NotFound

let charDiff (s1: char, s2: char): int =
    match s1 = s2 with
    | true -> 0
    | false -> 1

let distOne (s1: char list) (s2: char list): bool =
    List.zip s1 s2
    |> List.map charDiff
    |> List.sum
    |> (=) 1

let rec findOne (x: char list) (xs: char list list): Answer =
    match xs with
    | [] -> NotFound
    | head :: tail ->
        match distOne x head with
        | true -> Found (x, head)
        | false -> findOne x tail

let rec findAll (xs: char list list): char list * char list =
    match xs with
    | [] -> ([], [])
    | head :: tail ->
        match findOne head tail with
        | Found (x, y) -> (x, y)
        | NotFound _ -> findAll tail

let tupMatch (x: char, y: char): bool =
    x = y

let getMatch (s1: char list, s2: char list): char list =
    List.zip s1 s2
    |> List.filter tupMatch
    |> List.map fst

let getAnswer =
    File.ReadAllLines("input.txt")
    |> Array.map Seq.toList
    |> Array.toList
    |> findAll
    |> getMatch
    |> List.toArray
    |> System.String

printfn "String = %A" getAnswer
