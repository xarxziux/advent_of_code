open System
open System.IO

type coll = char Set * char Set * char Set

let first (c: coll): char Set =
    let (x, _, _) = c
    x

let second (c: coll): char Set =
    let (_, x, _) = c
    x

let third (c: coll): char Set =
    let (_, _, x) = c
    x

let parseChar (t: coll) (c: char): coll =
    match Set.contains c (third t) with
        | true -> (first t, second t, Set.remove c (third t))
        | false ->
            match Set.contains c (second t) with
            | true -> (first t, Set.remove c (second t), Set.add c (third t))
            | false ->
                match Set.contains c (first t) with
                | true -> (Set.remove c (first t), Set.add c (second t), third t)
                | false -> (Set.add c (first t), second t, third t)

let parseAllChars (str: string): bool * bool =
    let x: coll = List.fold parseChar (Set.empty, Set.empty, Set.empty) (Seq.toList str)
    (* printf "%A -> " x *)
    let y = (not (Set.isEmpty (second x)), not (Set.isEmpty (third x)))
    printfn "%A" y
    y

let addBools (x1: int, x2: int) (y: bool * bool): int * int =
    match y with
    | (false, false) -> (x1, x2)
    | (false, true) -> (x1, x2 + 1)
    | (true, false) -> (x1 + 1, x2)
    | (true, true) -> (x1 + 1, x2 + 1)

let getCheckSum (x: int, y: int): int = x * y

let answer =
    File.ReadAllLines("input.txt")
    |> Array.map parseAllChars
    |> Array.fold addBools (0, 0)
    |> getCheckSum

printfn "Checksum = %i" answer
