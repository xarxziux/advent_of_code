#load "data.fs"

open Day11

let fourthOfFour (_, _, _, x: 'a): 'a = x

let sumSquare (a: int[][]) (x: int, y: int, w: int): int * int * int * int =
    let ans: int  =
        a.[(y - 1) .. (y + w - 2)]
        |> Array.map (fun (n: int[]) -> Array.sum n.[(x - 1) .. (x + w - 2)])
        |> Array.sum
    (x, y, w, ans)

let maxSumAllSquares (a: int[][]) (w: int): int * int * int * int =
    let sumA = sumSquare a
    let wMax = Array.length a - w + 1
    [| for x in 1 .. wMax -> [| for y in 1 .. wMax -> sumA (x, y, w)|] |]
    |> Array.map (Array.maxBy fourthOfFour)
    |> Array.maxBy fourthOfFour

let maxEverything (a: int[][]): int * int * int * int =
    let fn = maxSumAllSquares a
    [| for x in 1 .. (Array.length a) -> fn x |]
    |> Array.maxBy fourthOfFour

let part1Answer = maxSumAllSquares input 3
let part2Answer = maxEverything input

printfn "Part 1 answer = %A" part1Answer
printfn "Part 2 answer = %A" part2Answer
