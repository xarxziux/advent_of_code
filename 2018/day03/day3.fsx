#load "data.fs"
open Data

let isWithin (p1: int, p2: int) (b1: int, b2: int, b3: int, b4: int): bool =
    (p1 >= b1) && (p1 < b3) && (p2 >= b2) && (p2 < b4)

let isWithinTwo (p: point) (bs: box list): bool =
    let pWithin = isWithin p
    let rec isWithinTwoRec (bs1: box list) (inOne: bool): bool =
        match bs1 with
        | [] -> false
        | head :: tail ->
            let isWithinNext: bool = pWithin head
            match isWithinNext && inOne with
            | true -> true
            | false -> isWithinTwoRec tail (isWithinNext || inOne)
    isWithinTwoRec bs false

let nextPoint (yMin: int, yMax: int) (p1: int, p2: int): point =
    match p2 < yMax with
    | true -> (p1, p2 + 1)
    | false -> (p1 + 1, yMin)

let boolToInt (b: bool): int = match b with true -> 1 | false -> 0

let countWithin (bl: box list) (xMin: int, yMin: int, xMax: int, yMax: int): int =
    let nextPointInner = nextPoint (yMin, yMax)
    let rec countWithinRec (p1: int, p2: int) (accum: int) =
        match p1 <= xMax with
        | false -> accum
        | true ->
            let accum': int = accum + (boolToInt <| isWithinTwo (p1, p2) bl)
            countWithinRec (nextPointInner (p1, p2)) accum'
    countWithinRec (xMin, yMin) 0

let findOrphan (ids: idAndBox list): int =
    let countWithinInner = countWithin <| List.map snd ids
    let rec findOrphanRec (ids': idAndBox list): int =
        match ids' with
        | [] -> 0
        | head :: tail ->
            match countWithinInner <| snd head with
                | 0 -> fst head
                | _ -> findOrphanRec tail
    findOrphanRec ids

let partOneAnswer: int = countWithin (List.map snd input) (0, 0, 1005, 1005)
let partTwoAnswer: int = findOrphan input

printfn "Number of overlapping points: %i" partOneAnswer
printfn "Singleton ID: %i" partTwoAnswer
